#!/bin/bash

echo 'setting up systray support for elementary OS'

sudo apt-get install -y libglib2.0-dev libgranite-dev \
    libindicator3-dev libwingpanel-dev indicator-application jq wget curl valac \
    gcc meson cmake build-essential git

if ! [ -e ~/.cache/indicator ]; then
    mkdir -p ~/.cache/indicator-git
fi

pushd .

cd ~/.cache/indicator-git || exit 1

git clone --depth=1 https://github.com/Lafydev/wingpanel-indicator-ayatana.git
cd wingpanel-indicator-ayatana || exit 1
meson build --prefix=/usr
cd build || exit 1
ninja
sudo ninja install

popd || exit 1

sudo cp ./systray-usersetup.sh /usr/local/bin/systray-usersetup
sudo chmod 755 /usr/local/bin/systray-usersetup
sudo cp systray-setup.service /etc/systemd/system/systray-setup.service

sudo systemctl enable --now systray-setup

echo 'finished setup'
