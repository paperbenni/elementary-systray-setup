


This is an easy deployment script for setting up systray icons on Elementary OS

register-users.sh needs to be run each time a new user which should have access to the functionality is added. 

```sh

git clone https://gitlab.com/paperbenni/elementary-systray-setup.git
cd elementary-systray-setup
sudo ./setup.sh
```
