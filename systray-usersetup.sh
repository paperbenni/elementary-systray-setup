#!/bin/bash

if ! [ "$(whoami)" = root ]; then
    echo "please run this script as root"
fi

for USERDIR in /home/*; do
    USERNAME="$(grep -o '[^/]*$' <<<"$USERDIR")"
    echo "username $USERNAME"
    if id -u "$USERNAME" &>/dev/null; then
        echo "setting up systray support for $USERNAME"
        sudo su "$USERNAME" -c 'mkdir -p ~/.config/autostart'
        sudo su "$USERNAME" -c 'cp /etc/xdg/autostart/indicator-application.desktop ~/.config/autostart/'
        sudo su "$USERNAME" -c "sed -i 's/^OnlyShowIn.*/OnlyShowIn=Unity;GNOME;Pantheon;/' ~/.config/autostart/indicator-application.desktop"
    fi
done

echo "done"
